require 'test_helper'

# class ApplicationHelperTest < ActionView::TestCase
#  test "full title helper" do
#   assert_equal full_title,         "Ruby on Rails Tutorial Sample App"
#   assert_equal full_title("Help"), "Help | Ruby on Rails Tutorial Sample App"
#  end
# end

# module ApplicationHelper

 # Returns the full title on a per-page basis.
 def full_title(page_title = '')
   base_title = "Ruby on Rails Tutorial Sample App"
   if page_title.empty?
     base_title
   else
     page_title + " | " + base_title
   end
 end
